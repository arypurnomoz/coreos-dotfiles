#!/bin/bash

DOTFILES=/home/core/dotfiles

function install() {
  ln -sf $DOTFILES/alias.sh $1/.bash_profile
  ln -sf $DOTFILES/vimrc $1/.vimrc
  ln -sf $DOTFILES/ssh_config $1/.ssh/config
}

install /root
install /home/core
